**1. Explain the x86 registers being used in the assembly (we also explained some more)**</br>

R prefix are 64-bit registers, below are callee-save registers. <br/>
RBP - Register base pointer that points to the base of the current stack frame <br/>
RBX - Register b extended, base register that used to address memory or hold data <br/>
RDI - Destination index register for string operators. <br/>
RSI - Source index register for string operators. <br/>
RSP -  Register stack pointer that points to the top of the current stack frame.

E prefix are 32-bit register, EAX is caller-save register. <br/>
EAX - Accumulator register that stores return values from function; also general register for arithmetic operations <br/>
EBX - Base register that used to address memory or hold data <br/>
EDI - Destination index register for string operators. <br/>
ESI - Source index register for string operators.

**2. Explain each and every single line of the ASM program** <br/>
```
sum2:                                   # @sum2
//rdi holds the value of int a while rsi holds the value of int b, this line adds the two values up and load it to eax 
       lea     eax, [rdi + rsi]
 
//return the sum2
       Ret
 
print_the_value:                        # @print_the_value
//edi holds parameter value but the function also need to print the sign so move edi to esi so the sign can be in edi
       mov     esi, edi
 
//move the address of the sign of the value to edi
       mov     edi, offset .L.str
 
//zero the contents of eax in case that the function has variable arguments
       xor     eax, eax
 
//call printf on the parameter
       jmp     printf                  # TAILCALL
 
entry_point:                            # @entry_point
//push rbx (64-bit) to push the value that is currently stored in rbx into the stack
       push    rbx
 
//call rand on int a and store value to eax
       call    rand
 
//move int a value stored in eax to ebx so it can be passed as parameter for sum2
       mov     ebx, eax
 
//call rand on int b and store value to eax 
       call    rand
 
//move int b value stored in eax to edi so it can be passed as parameter for sum2
       mov     edi, eax
 
//move int a value in ebx to esi so it can be passed as parameter for sum2
       mov     esi, ebx
 
//call sum2 on edi and esi and the result is stored to eax
       call    sum2
 
//move eax to edi so it can be passed as a parameter to print
       mov     edi, eax
 
//restore rbx (64 bits) by remove value from the stack
       pop     rbx
 
//jump to print_the_value function to run it
       jmp     print_the_value         # TAILCALL
 
 
//Use for the sign of value parameter in print_the_value
.L.str:
       .asciz  "%d"
```
